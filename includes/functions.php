<?php
 	function confirm_query($result_set){
 		if(!$result_set)
 			die("Database query failed");
 	}
	
	function form_errors($errors=array()){
		$output = "";
		if (!empty($errors)){
			$output .=  "<div class=\"error\">";
			//$output .=  "Please Fix The following errors:";
			$output .=  "<ul class =\"list\">";
			foreach ($errors as $key => $error) {
				$output .= "<li>";
				$output .=htmlentities($error);
				$output .="</li>";
			}
			$output .= "</ul>";
			$output .= "</div>";

			}
			return $output ;
	}	

 	function find_all_subjects(){
 		global  $connection;
		$query = "SELECT * ";
		$query .= "FROM Subjects ";
		//$query .= "WHERE visible = 1 ";
		$query .= "ORDER BY position ASC";
		$subject_set = mysqli_query($connection, $query);
		confirm_query($subject_set);
		return  $subject_set;	
 	}

 	function find_all_pages_for_subject($subjectID){
		global $connection;
		$subjectID = mysqli_real_escape_string($connection, $subjectID);
 		$query = "SELECT * ";
		$query .= "FROM Pages ";
		$query .= "WHERE visible = 1 ";
		$query .= "AND subject_id = {$subjectID} "; 
		$query .= "ORDER BY position ASC";
		$page_set=mysqli_query($connection, $query);
		confirm_query($page_set);
		return $page_set;
 	}

 	function find_subject_by_id($subject_id){
 		global  $connection;
 		//Escape special characters in a string for use in an SQL statement.
 		$safe_subject_id = mysqli_real_escape_string($connection, $subject_id);
		$query = "SELECT * ";
		$query .= "FROM Subjects ";
		$query .= "WHERE id = {$safe_subject_id} ";
		$query .= "LIMIT 1";
		$subject_set = mysqli_query($connection, $query);
		confirm_query($subject_set);
		/* fetch the subject_set as a associative arrat */
		if($subject = mysqli_fetch_assoc($subject_set)){
			return  $subject;
		}
		else{
			return null;
		}
	}

	function find_page_by_id($page_id){
		global $connection;
		$safe_page_id = mysqli_real_escape_string($connection, $page_id);
		$query = "SELECT * ";
		$query .= "FROM Pages ";
		$query .= "WHERE id = {$safe_page_id} ";
		$query .= "LIMIT 1";

		$page_set = mysqli_query($connection, $query);
		confirm_query($page_set);
		/* fetch the page_set as a associative arrat */
		if($page = mysqli_fetch_assoc($page_set)){
			return  $page;
		}
		else{
			return null;
		}

	}

	function find_selected_page(){
		global $current_subject;
		global $current_page;

		if(isset($_GET["subject"])){
			$current_subject = find_subject_by_id($_GET["subject"]);
			$current_page=NULL;
		}
		else if(isset($_GET["page"])){
			$current_page = find_page_by_id($_GET["page"]);
			$current_subject = NULL;
		}
		else{
			$current_subject = NULL;
			$current_page = NULL;
		}

	}

	function redirect_to($new_location){
		header("Location: " . $new_location);
		exit;
	}

	function mysql_prep($menu_name){
		global $connection;
		return mysqli_real_escape_string($connection, $menu_name);
	}
 	// navigation takes 2 arguments
 	// -The currently  subject array or null 
 	// -The currently  page array or null 
 	function navigation($subject_array, $page_array) {
 		$output = "<ul class=\"subjects\">"; 
		$subject_set = find_all_subjects();
		while($subject = mysqli_fetch_assoc($subject_set)){
				$output .= "<li ";
				if($subject_array && $subject_array["id"]==$subject["id"])
				 	$output .= "class=\"selected\""; 
				$output .= ">";
				$output .= "<a href=\"manage_content.php?subject=";
				$output .= urlencode($subject['id']);
				$output .= "\">"; 
				$output .= htmlentities($subject["menu_name"]);
				$output .= "</a>";
					
					
				$page_set=find_all_pages_for_subject($subject["id"]);	
					
				$output .= "<ul class=\"pages\">";
				while($page = mysqli_fetch_assoc($page_set)) {
					$output .=  "<li ";
					if($page_array && $page_array["id"]==$page["id"])
						$output .=  " class=\"selected\""; 
					$output .=  ">";
					$output .= "<a href=\"manage_content.php?page=";
					$output .=  urlencode($page["id"]);
					$output .="\">";
					$output .=  htmlentities($page["menu_name"]); 
					$output .="</a> </li>";

				} 
				mysqli_free_result($page_set); 			  	
				$output .= "</ul></li>";
				
			}
		mysqli_free_result($subject_set);			 
		$output .="</ul>";

		return $output;
 	}
 	
 ?>		
 
