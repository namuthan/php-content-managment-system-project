<?php require_once("../includes/session.php"); ?>
<!-- //include the required data base connection file. -->
<?php require_once("../includes/db_connection.php"); ?>
<!-- //include the required functions. -->
<?php require_once("../includes/functions.php"); ?>
<!--include the require layout files -->
<?php include("../includes/layouts/header.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php find_selected_page();?>


<?php 
	//process the data beign submitted. 
	if (isset($_POST['submit'])){
		//process the form:
		$subject_id = $_GET["Subject"];//(int)$_POST["subject"];
		$menu_name = mysql_prep($_POST["menu_name"]);
		$position = (int)$_POST["position"];
		$visible = (int)$_POST["visible"];
		$content = mysql_prep($_POST["content"]);
		$required_fields  = array("menu_name", "position", "visible", "content");
		validate_presence($required_fields);
		$fields_with_max_lengths =  array('menu_name' => 30);
		validate_max_lengths($fields_with_max_lengths);
		if(!empty($errors)){
			$_SESSION["errors"] = $errors;
			//redirect_to("new_page.php");
		}
		else{
			$query = "INSERT INTO Pages (";
			$query .= "subject_id, menu_name, position, visible, content";
			$query .= ") VALUES (";
			$query .= " {$subject_id},'{$menu_name}', {$position}, {$visible}, '{$content}' ";
			$query .= ")";
			$result = mysqli_query($connection, $query);

			if($result){
				//success
				$_SESSION["message"]=  "Page created";
				redirect_to("manage_content.php");
			}
			else{
				//failure
				$_SESSION["message"]= "Page creation failed";
				redirect_to("new_page.php");
			}
		}
	}
	else{ ?>
		<div id="main">
			<div id="navigation">
				<?php echo navigation($current_subject, $current_page)?>
			</div>
			<div id="page">
				<?//php echo message(); ?>
				<h2>Create Page<h2>
				<?//php $errors = errors(); ?>
				<?//php echo form_errors($errors); ?>

				<form class="list" action="new_page.php" method="post">
					<p>Menu Name<span style=color:red>(required)</span>:
					  <input type="text" name="menu_name" value="" />
					</p>
					<p>Position 
						<select name = "position">
						  <?php 
						  	//Find how many rows are in the pages of a specific subject.
						  	$page_set = find_all_pages_for_subject($_GET["subject"]);
						  	$page_count = mysqli_num_rows($page_set);
						  	for($count = 1; $count <= ($page_count+1); $count++){
						  	  echo "<option value=\"{$count}\">{$count}</option>";
						  	}
						  ?>
						</select>
					</p>
					<p>Visible
						<input type="radio" name="visible" value="0"/>No
						&nbsp;
						<input type="radio" name="visible" value="1"/>Yes
					</p>
					<br />
					<label for="content"><p>Content</p></label>
					<textarea name="content" id= "content" rows="20" cols="90" value=""></textarea><br />
					<input type="submit" name = "submit" value = "Create Subject">
				</form>
				<br />
				<a href="manage_content.php">Cancel </a>
			</div>
		 </div>

		 <?php } ?>

		 




 <?php include("../includes/layouts/footer.php"); ?>

 
